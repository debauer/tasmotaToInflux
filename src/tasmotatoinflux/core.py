from __future__ import annotations

import json
import logging
from argparse import ArgumentParser, Namespace
from logging import getLogger
from typing import Any

import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient
from paho.mqtt.client import MQTTMessage
from paho.mqtt.enums import CallbackAPIVersion

from tasmotatoinflux.config import DEVICES, INFLUX_DB, MQTT_BROKER
from tasmotatoinflux.config_wrapper.types import Device
from tasmotatoinflux.config_wrapper.wrapper import ConfigWrapper
from tasmotatoinflux.points import InfluxPoint

_log = getLogger("default")
ch = logging.StreamHandler()
_log.addHandler(ch)


def parse() -> Namespace:
    parser = ArgumentParser(description="System to record the data on a trimodal crane")
    parser.add_argument(
        "-d",
        "--dryrun",
        action="store_const",
        const="dryrun",
        help="don't commit to influxdb",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        const="verbose",
        help="verbose",
    )
    return parser.parse_args()


def core() -> None:  # noqa: C901
    def on_tele_message(_mosq: Any, _obj: Any, message: MQTTMessage) -> None:  # noqa: ANN401
        topic_split = message.topic.split("/")
        if topic_split[4] == "SENSOR":
            jd: Any = json.loads(message.payload)
            if isinstance(jd, dict):
                points = InfluxPoint(topic_split, jd, config)
                _log.debug(points)
                if not dryrun:
                    try:
                        influxc.write_points(points.datapoints)
                    except Exception as e:  # noqa: BLE001
                        _log.info(
                            f"error on name: {topic_split[3]} topic: {message.topic}",
                        )
                        _log.debug(f"{e}")

    def on_config(_mosq: Any, _obj: Any, message: MQTTMessage) -> None:  # noqa: ANN401
        pl = json.loads(message.payload)
        device_name = pl["dn"]
        host_name = pl["hn"]
        for device_id, d in enumerate(DEVICES):
            if d.device_name == host_name:
                DEVICES[device_id] = Device(host_name, device_name)
                print(f"Update {host_name} {device_name}")  # noqa: T201
                return
        print(f"add {host_name} {device_name}")  # noqa: T201
        DEVICES.append(Device(host_name, device_name))

    def on_generic(
        _mosq: Any,  # noqa: ANN401
        _obj: Any,  # noqa: ANN401
        _message: MQTTMessage,
    ) -> None:
        pass
        # print("generic:" + message.topic)

    config = ConfigWrapper(MQTT_BROKER, INFLUX_DB, DEVICES)

    args = parse()
    _log.setLevel(logging.INFO)
    if args.verbose:
        _log.setLevel(logging.DEBUG)
    dryrun = args.dryrun

    if not dryrun:
        influxc = InfluxDBClient(
            config.influxdb.address,
            config.influxdb.port,
            config.influxdb.user,
            config.influxdb.password,
            config.influxdb.database_name,
        )
    mqttc = mqtt.Client(callback_api_version=CallbackAPIVersion.VERSION1)
    mqttc.message_callback_add("+/+/tele/+/#", on_tele_message)
    mqttc.message_callback_add("tasmota/discovery/+/config", on_config)
    mqttc.on_message = on_generic
    mqttc.connect(config.mqtt.address, config.mqtt.port, 60)
    mqttc.subscribe("#", 0)
    mqttc.loop_forever()


if __name__ == "__main__":
    core()
